package id.co.softorb;

import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mob_pcb_litro_bri.R;
import com.hoho.android.usbserial.driver.CdcAcmSerialDriver;
import com.hoho.android.usbserial.driver.ProbeTable;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import id.co.softorb.lib.passti.STIUtility;

import static android.content.ContentValues.TAG;
import static id.co.softorb.lib.helper.ErrorCode.ERR_UNKNOWN;
import static id.co.softorb.lib.helper.ErrorCode.OK;

public class MainActivity extends AppCompatActivity {

    STIUtility sti;

    TextView tv;
    Button initReader, initPower, cekBalance, deduct;
    EditText nominal;

    UsbManager usbManager=null;
    UsbDevice usbDevice=null;
    PendingIntent permissionIntent;
    UsbSerialDriver driver;
    private boolean modeOtg = false;
    ProbeTable customTable;

    private final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    int trxcounter;
    int brirefno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getUsbPermission();

        trxcounter=0;
        brirefno=0;

        tv=findViewById(R.id.tv);
        initReader=findViewById(R.id.initReader);
        cekBalance=findViewById(R.id.cekbalance);
        deduct=findViewById(R.id.deduct);
        nominal=findViewById(R.id.nominal);

        initReader.setOnClickListener(clickInitReader);
        cekBalance.setOnClickListener(clickCekBalance);
        deduct.setOnClickListener(clickDeduct);

    }

    private void getUsbPermission() {
        if(usbManager == null) {
            usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        }
        if(permissionIntent==null){
            permissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
            IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
            registerReceiver(usbReceiver, filter);
        }
        customTable = new ProbeTable();
        customTable.addProduct(1241, 46388, CdcAcmSerialDriver.class);

        UsbSerialProber prober = new UsbSerialProber(customTable);
        List<UsbSerialDriver> drivers = prober.findAllDrivers(usbManager);

        driver = drivers.get(0);
        if (usbDevice == null) {
            usbDevice = driver.getDevice();
            Log.d("vendorId",""+driver.getDevice().getVendorId());
            Log.d("productId",""+driver.getDevice().getProductId());
        }
        if (!usbManager.hasPermission(usbDevice)) {
            Log.d("masukpermission", "1");
            usbManager.requestPermission(usbDevice, permissionIntent);
        }
    }

    private final BroadcastReceiver usbReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(ACTION_USB_PERMISSION == intent.getAction()){
                synchronized (this){
                    UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if(intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)){
                        if(device!=null){
                            usbDevice=device;
                            Log.d("Permission allowed", ""+device);
                            if(modeOtg) getUsbPermission();
                        }
                    }
                    else {
                        Log.d("Permission denied", ""+device);
                    }
                }
            }
        }
    };

    View.OnClickListener clickInitReader=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int result=ERR_UNKNOWN;
            if(sti==null){
                sti=new STIUtility(getApplicationContext());
                result=sti.initReader(usbManager, usbDevice, driver);
            }
        }
    };

    View.OnClickListener clickCekBalance=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                int result=ERR_UNKNOWN;
                result=sti.cekBalance();
                Log.d("resultcekbal1", ""+result);
                if(result==OK){
                    Locale locale = new Locale("id", "ID");
                    NumberFormat format=NumberFormat.getCurrencyInstance(locale);
                    showText("Saldo : "+format.format(sti.getBalance()));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    View.OnClickListener clickDeduct=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d("clickcekdeduct", "1");
            trxcounter++;
            brirefno++;
            Log.d("trxcounterr", ""+trxcounter);
            Log.d("brirefno", ""+brirefno);
            sti.setTrxCounter(trxcounter);
            String strBRIRef = String.format("%06d",brirefno);
            Log.d(TAG,"strBRIRef "+strBRIRef);
            sti.setBRIRefNo(strBRIRef);
            int result=ERR_UNKNOWN;
            String i = String.valueOf(nominal.getText());
            result=sti.deduct(Integer.parseInt(i));
            Log.d("resultdeduct1", ""+result);
            if(result==OK){
                Locale locale = new Locale("id", "ID");
                NumberFormat format=NumberFormat.getCurrencyInstance(locale);
                showText("Saldo Awal : "+format.format(sti.getBalance())+"\nSaldo Akhir : "+format.format(sti.getDeduct()));
            }
        }
    };

    private void showText(final String text)
    {
        new Thread() {
            public void run() {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tv.setText(text);
                        }
                    });
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }.start();
    }
}