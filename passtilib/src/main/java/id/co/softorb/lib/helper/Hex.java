package id.co.softorb.lib.helper;

import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialPort;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public final class Hex {
    public static String byteToHexString(byte b) {
        int n = b & 0x000000FF;
        String result = (n < 0x00000010 ? "0" : "") + Integer.toHexString(n);
        return result.toUpperCase();
    }
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len/2];

        for(int i = 0; i < len; i+=2){
            data[i/2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));
        }

        return data;
    }

    public static String bytesToHexString(byte[] bArray) {
        if (bArray == null){
            return null;
        }
        StringBuffer sb = new StringBuffer(bArray.length);

        String sTemp;
        int j = 0;
        for (int i = 0; i < bArray.length; i++) {
            sTemp = Integer.toHexString(0xFF & bArray[i]);
            if (sTemp.length() < 2)
                sb.append(0);

            sb.append(sTemp.toUpperCase());
            j++;

        }
        return sb.toString();
    }

    public static int sum(byte[] array) {
        int result = 0;
        for (final byte v : array) {
            result += v;
        }
        return result;
    }

    public static String decodeL(byte[] data){
        Log.d("dowrite", "data="+bytesToHexString(data));
        StringBuilder sb=new StringBuilder();
//        String str=new String(decodeByte(data), Charsets.UTF_8);
        String str=new String(decodeByte(data));
        Log.d("str", ""+str);
//            Log.d("decodebytecharset", ""+sb.append(str).toString());
        return sb.append(str).toString();
//        return str;
//        try {
//
////            return bytesToHexString(decodeByte(data));
//        }catch (Exception e){
//            return null;
//        }
    }

    public static byte[] encodeL(String data) {
        ByteArrayOutputStream bos= new ByteArrayOutputStream();
//        byte[] ba = hexStringToByteArray(data);
        byte[] ba=data.getBytes();
//        Log.d("data",""+data+" "+Hex.bytesToHexString(ba));
        try {
            bos.write(ba);
            int cursum = Hex.sum(ba) % 256;
//            Log.d("cursum", ""+cursum);
            int h = 256 - cursum;
            bos.write(new byte[]{(byte)h});
            bos.write(0);
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bos.toByteArray();
    }

    public static String bytesToHexString(byte[] text, int offset, int length) {
        return bytesToHexString(text, offset, length, 1000);
    }

    public static String bytesToHexString(byte[] text, int offset, int length, int numRow) {
        if(text == null) return "NULL";
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < length; i++) {
            if(i != 0 && i % numRow == 0) result.append("\n");
            result.append(byteToHexString(text[offset + i]));
        }
        return result.toString();
    }

    public static byte[] doWrite(byte[] bytes, UsbSerialPort port) {
        try {
            Log.d("portt"," "+port);
            int t = port.write(bytes, bytes.length * 50);
            Thread.sleep(500);
            byte[] retBa = new byte[128];
            int len = port.read(retBa, 128 * 50);
            byte[] ret = Arrays.copyOfRange(retBa, 0, len);
            return ret;
        }catch (Exception e){
            Log.d("error", ""+e);
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] decodeByte(byte[] data){
        try {
            if(data==null){
                return null;
            }
            int cursum=0;
            for(int b=0; b<data.length-3;b++){
                cursum +=data[b];
            }
//            int sum = flipbits(data[data.length - 2] - 1);

//            ByteArrayInputStream bo = new ByteArrayInputStream(data, 0, data.length - 2);

            //            byte[] abc=;
//            Log.d("abcdefghi", ""+bytesToHexString(abc));

            InputStream is = new ByteArrayInputStream(data, 0, data.length - 2);
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] datas = new byte[4];

            while ((nRead = is.read(datas, 0, datas.length)) != -1) {
                buffer.write(datas, 0, nRead);
            }

            buffer.flush();
            byte[] targetArray = buffer.toByteArray();
            Log.d("Targetarray", ""+bytesToHexString(targetArray));
            return targetArray;
        }catch (Exception e){
            return null;
        }
    }

    public static int flipbits(int i) {
        String bitString=Integer.toBinaryString(i);
        if (bitString.length() % 4 != 0) {
            bitString = ("0000" + bitString).substring(bitString.length() % 4);
        }
        for(int io=0; io<bitString.length()-1;io++){
            StringBuilder stringBuilder = new StringBuilder(bitString);
            stringBuilder.setCharAt(io, (char)(stringBuilder.charAt(io) == '0' ? '1' : '0'));
            bitString=stringBuilder.toString();
        }
        return Integer.parseInt(bitString, 2);
    }
}
