package id.co.softorb.lib.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Util {
    public static String dateNow(String dateFormat) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(cal.getTime());
    }


}
