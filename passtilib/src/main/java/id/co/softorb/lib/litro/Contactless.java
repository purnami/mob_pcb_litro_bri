package id.co.softorb.lib.litro;

import android.content.Context;
import android.hardware.usb.UsbDeviceConnection;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialPort;

import java.io.IOException;

import id.co.softorb.lib.helper.Hex;

import static android.content.ContentValues.TAG;
import static id.co.softorb.lib.helper.Hex.bytesToHexString;
import static id.co.softorb.lib.helper.Hex.decodeL;
import static id.co.softorb.lib.helper.Hex.doWrite;
import static id.co.softorb.lib.helper.Hex.encodeL;
import static id.co.softorb.lib.helper.Hex.hexStringToByteArray;

public class Contactless {
    Context ctx;
    UsbSerialPort port = null;
    UsbDeviceConnection connection=null;
    private boolean modeOtg = false;
    private NfcAdapter nfcAdapter;

    public Contactless(Context ctx){
        this.ctx=ctx;
    }

    public int init(UsbSerialPort usbSerialPort, UsbDeviceConnection connection){
        nfcAdapter=((NfcManager) ctx.getSystemService(Context.NFC_SERVICE)).getDefaultAdapter();
        if (nfcAdapter == null) {
            modeOtg = true;
        }
        this.port=usbSerialPort;
        this.connection=connection;
        setupPort(this.port, this.connection);
        return 0;
    }

    private String transceive(byte[] input){
        return decodeL(doWrite(input, port));
    }

    private void setupPort(UsbSerialPort usbSerialPort, UsbDeviceConnection connection) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                port=usbSerialPort;
                try {
                    port.open(connection);
                    port.setParameters(38400,
                            UsbSerialPort.DATABITS_8,
                            UsbSerialPort.STOPBITS_1,
                            UsbSerialPort.PARITY_NONE);
                    Log.d("portt1"," "+port);
                    byte[] a = encodeL("10");
                    Log.d("encodeL",""+bytesToHexString(a));
                    byte[] b= doWrite(a, port);
                    Log.d("doWrite", ""+bytesToHexString(b));
                    String c= decodeL(b);
                    Log.d("decodeL", ""+c);

                    if(!c.contains("9000")){
                        Log.d("masuk error", "1");
                        throw new IOException("INIT NFC FAILED");
                    }
//                    a = encodeL("00A4040008A000000571504805");
//                    Log.d("encodeL2",""+bytesToHexString(a));
//                    b= doWrite(a, port);
//                    Log.d("doWrite2", ""+bytesToHexString(b));
//                    c= decodeL(b);
//                    Log.d("decodeL2", ""+c);
//
//                    if(!c.contains("9000")){
//                        Log.d("masuk error", "2");
////                        throw new IOException("INIT NFC FAILED");
//                    }
                    Log.d(TAG,"RDR-->CTL : 00A4040008A000000571504805");
                    sendCTL(hexStringToByteArray("00A4040008A000000571504805"));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public byte[] sendCTL(byte[] apdu){
        String ret = transceive(encodeL("30" + Hex.bytesToHexString(apdu).trim()));
        Log.d(TAG,"RDR<--CTL : "+ ret);
        return hexStringToByteArray(ret);
    }
}
