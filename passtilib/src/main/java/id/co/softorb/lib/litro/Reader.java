package id.co.softorb.lib.litro;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialDriver;

import id.co.softorb.lib.helper.Hex;

import static android.content.ContentValues.TAG;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;

public class Reader {
    Context ctx;
    Usb usb;
    Contactless contactless;
    Sam sam;

    public Reader(Context ctx){
        this.ctx=ctx;
        usb=new Usb(ctx);
        contactless=new Contactless(ctx);
        sam=new Sam(ctx);
    }

    public int init(UsbManager usbManager, UsbDevice usbDevice, UsbSerialDriver driver){
        Log.d("masuk","1");
        int result=NOT_OK;
        result = usb.init(usbManager, usbDevice, driver);
        Log.d("resultinitusb", ""+result);
        if(result!=OK){
            return result;
        }
        Log.d("masukctl", "1");
        result = contactless.init(usb.getUsbSerialPort(), usb.getConnection());
        Log.d("resultinitctl", ""+result);
        if(result!=OK){
            return result;
        }
        result = sam.init(usb.getUsbSerialPort(), usb.getConnection());
        Log.d("resultinitsam", ""+result);
        if(result!=OK){
            return result;
        }
        return OK;
    }

    public byte[] sendCTL(byte[] apdu) {
        Log.d(TAG,"RDR-->CTL : "+ Hex.bytesToHexString(apdu));
        return contactless.sendCTL(apdu);
    }

    public byte[] sendSAM(byte[] apdu) {
        Log.d(TAG,"RDR-->SAM : "+ Hex.bytesToHexString(apdu));
        return sam.sendSAM(0, apdu);
    }
}
