package id.co.softorb.lib.litro;

import android.content.Context;
import android.hardware.usb.UsbDeviceConnection;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialPort;

import id.co.softorb.lib.helper.Hex;

import static android.content.ContentValues.TAG;
import static id.co.softorb.lib.helper.Hex.decodeL;
import static id.co.softorb.lib.helper.Hex.doWrite;
import static id.co.softorb.lib.helper.Hex.encodeL;
import static id.co.softorb.lib.helper.Hex.hexStringToByteArray;

public class Sam {
    Context ctx;
    boolean readidContinue = false;
    UsbSerialPort port = null;
    UsbDeviceConnection connection=null;
    private boolean modeOtg = false;
    private NfcAdapter nfcAdapter;

    public Sam(Context ctx) {
        this.ctx = ctx;
    }

    public int init(UsbSerialPort usbSerialPort, UsbDeviceConnection connection) {
        nfcAdapter=((NfcManager) ctx.getSystemService(Context.NFC_SERVICE)).getDefaultAdapter();
        if (nfcAdapter == null) {
            modeOtg = true;
        }
        readidContinue=false;
        this.port=usbSerialPort;
        this.connection=connection;
        return 0;
    }

    private String transceive(byte[] input){
        return decodeL(doWrite(input, port));
    }

    public byte[] sendSAM(int port, byte[] apdu) {
        String ret = transceive(encodeL(("31" + Hex.bytesToHexString(apdu).trim())));
        Log.d(TAG,"RDR<--SAM : "+ ret);
        return hexStringToByteArray(ret);
    }
}
