package id.co.softorb.lib.litro;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;

import static id.co.softorb.lib.helper.ErrorCode.OK;

public class Usb {
    Context ctx;

    UsbSerialPort port = null;

    private UsbDeviceConnection connection;
    private UsbSerialPort usbSerialPort;

    public UsbDeviceConnection getConnection() {
        return connection;
    }

    public void setConnection(UsbDeviceConnection connection) {
        this.connection = connection;
    }

    public UsbSerialPort getUsbSerialPort() {
        return usbSerialPort;
    }

    public void setUsbSerialPort(UsbSerialPort usbSerialPort) {
        this.usbSerialPort = usbSerialPort;
    }

    public Usb(Context ctx){
        this.ctx=ctx;
    }

    public int init(UsbManager usbManager, UsbDevice usbDevice, UsbSerialDriver driver){
        connection = usbManager.openDevice(usbDevice);
        setConnection(connection);
        if (connection == null) {
            port = null;
        }
        usbSerialPort = driver.getPorts().get(0);
        setUsbSerialPort(usbSerialPort);
        return OK;
    }

}
