package id.co.softorb.lib.passti;

import android.content.Context;
import android.util.Log;

import java.util.Arrays;

import id.co.softorb.lib.helper.APDUHelper;
import id.co.softorb.lib.helper.Converter;
import id.co.softorb.lib.helper.ErrorCode;
import id.co.softorb.lib.helper.Hex;
import id.co.softorb.lib.helper.Util;
import id.co.softorb.lib.litro.Params;
import id.co.softorb.lib.litro.Reader;

import static android.content.ContentValues.TAG;
import static id.co.softorb.lib.helper.ErrorCode.BRI_CTL_ERR_DESFIRECOMMITTRX;
import static id.co.softorb.lib.helper.ErrorCode.BRI_CTL_ERR_DESFIREDEBIT;
import static id.co.softorb.lib.helper.ErrorCode.BRI_CTL_ERR_DESFIREWRITELASTTRX;
import static id.co.softorb.lib.helper.ErrorCode.BRI_CTL_ERR_DESFIREWRITELOG;
import static id.co.softorb.lib.helper.ErrorCode.BRI_SAM_ERR_DESFIRECREATEHASH;
import static id.co.softorb.lib.helper.ErrorCode.CTL_ERR_INSUFFICIENTBALANCE;
import static id.co.softorb.lib.helper.ErrorCode.ERR_NO_RESP;
import static id.co.softorb.lib.helper.ErrorCode.ERR_SW1SW2;
import static id.co.softorb.lib.helper.ErrorCode.NOT_OK;
import static id.co.softorb.lib.helper.ErrorCode.OK;
import static id.co.softorb.lib.passti.devinfo.issuer;

public class BRI {
    Context ctx;
    Reader reader;
    byte[] random;
    String tagclass;
    private byte[] lasttrxdate=new byte[3];
    private byte[] akumdebet=new byte[4];
    private long lAkumDebet=0;
    private byte[] bBalanceKartu=new byte[4];
    private int lBalanceKartu=0;
    private String balance;
//    private String saldoAwal;

    int saldoAwal=0;
    int saldoAkhir=0;
    private byte[] baAmount=new byte[4];
    private String hash_cardno,timestamp,hash_amount,hash_refno,hash_batchno="";
    private String refno;
    private byte[] samHash=new byte[4];

    public byte[] getCardNo() {
        return cardNo;
    }

    public void setCardNo(byte[] cardNo) {
        this.cardNo = cardNo;
    }

    byte[] cardNo;

    public byte[] getRandom() {
        return random;
    }

    public void setRandom(byte[] random) {
        this.random = random;
    }

    private byte[] rapdu=new byte[256];

    byte[] uid= Hex.hexStringToByteArray("04136F42815780");
//    String strBRIRef = String.format("%06d",brirefno);
    byte[] cmddata = {(byte)0xFF,(byte)0x00,(byte)0x00,(byte)0x03,(byte)0x00,(byte)0x80,(byte)0x00,(byte)0x00,(byte)0x00};


    private static String CTL_SELECT_AID_DESFIRE = "905A00000301000000";
    private static String CTL_READ_CARD_HOLDER = "90BD0000070000000000000000";
    private static String CTL_READ_CI_STATUS = "90BD0000070100000000000000";
    private static String CTL_SELECT_AID_3000000 = "905A00000303000000";
    private static String CTL_SELECT_AID_5000000 = "905A00000305000000";
    private static String CTL_GET_KEY_CARD = "900A0000010000";
    private static String SAM_SELECT_AID = "00A4040009A00000000000000011";
    private static String UID = "04136F42815780";

    public BRI(Context ctx, Reader reader) {
        this.ctx = ctx;
        this.reader = reader;
        tagclass= this.getClass().getSimpleName();
    }

    public int cekBalance() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] random = new byte[8];
        int result;
        result=CTL_ReadCardHolderDesfire();
        if(result!= OK)
        {
            return ErrorCode.BRI_CTL_ERR_DESFIREGETCARDNO;
        }

        result =CTL_ReadCIStatus();
        if(result!= OK)
        {
            return ErrorCode.BRI_CTL_ERR_DESFIREGETCARDSTATUS;
        }

        result =CTL_SelectAID_3000000();
        if(result!= OK)
        {
            return ErrorCode.BRI_CTL_ERR_DESFIRESELECTAID3;
        }

        result =CTL_GetKeyCard();
        if(result!= OK)
        {
            return ErrorCode.BRI_CTL_ERR_DESFIREGETKEYCARD;
        }

        result=SAM_SelectAID();
        if(result!= OK)
        {
            return ErrorCode.BRI_SAM_ERR_SELECTAID;
        }

        result=SAM_GetRandom();
        if(result!= OK){
            return ERR_NO_RESP;
        }

        result=CTL_AuthDesfire(getRandom());
        if(result!= OK){
            return ERR_NO_RESP;
        }

        result = CTL_GetLastTrx();
        if(result!= OK){
            return ERR_NO_RESP;
        }

        result =CTL_GetBalanceDesfire();
        if(result!= OK){
            return ERR_NO_RESP;
        }

        saldoAwal=lBalanceKartu;
        return 0;
    }

    private int CTL_GetBalanceDesfire() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        byte[] cmdGetBalanceBRIDesfire = {(byte) 0x90,(byte) 0x6C,(byte) 0x00,(byte) 0x00,(byte) 0x01,(byte) 0x00,(byte)0x00};

        rapdu = reader.sendCTL(cmdGetBalanceBRIDesfire);
        if(rapdu == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(rapdu) != OK){
            return NOT_OK;
        }

        bBalanceKartu=Converter.ChangeEndian(rapdu,4);
        lBalanceKartu=Converter.ByteArrayToInt(bBalanceKartu,0,bBalanceKartu.length, Converter.BIG_ENDIAN);
//        balance = String.valueOf(lBalanceKartu);
        return OK;
    }

    private int CTL_GetLastTrx() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmd = new byte[]{-112, -67, 0, 0, 7, 3, 0, 0, 0, 7, 0, 0, 0};
        byte[] lasttrxdate = new byte[3];
        rapdu = reader.sendCTL(cmd);
        if(rapdu==null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(rapdu) != 0){
            return ERR_SW1SW2;
        }

        byte[] akumdebet = new byte[4];
        System.arraycopy(rapdu, 0, lasttrxdate, 0, 3);
        setLastTrxDate(lasttrxdate);
        System.arraycopy(rapdu, 3, akumdebet, 0, 4);
        setAkumDebet(akumdebet);

        return OK;
    }

    private int setAkumDebet(byte[] akumdebet) {
        if(akumdebet.length!=4)
            return NOT_OK;
        this.akumdebet=akumdebet;
        this.lAkumDebet= Converter.ByteArrayToInt(akumdebet,0,4,Converter.BIG_ENDIAN);
        return OK;
    }

    private int setLastTrxDate(byte[] lasttrxdate) {
        if(lasttrxdate.length!=3)
            return NOT_OK;
        this.lasttrxdate=lasttrxdate;
        return OK;
    }

    private int SAM_SelectAID() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] command = new byte[]{0, -92, 4, 0, 9, -96, 0, 0, 0, 0, 0, 0, 0, 17};
        rapdu=reader.sendSAM(command);
        if(rapdu==null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(rapdu) != 0){
            return ERR_SW1SW2;
        }
        return OK;
    }

    private int CTL_GetKeyCard() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmd = new byte[]{-112, 10, 0, 0, 1, 0, 0};
        rapdu=reader.sendCTL(cmd);
        if(rapdu==null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(rapdu) != 0){
            return ERR_SW1SW2;
        }
        random=Arrays.copyOfRange(rapdu, 0, rapdu.length-2);
//        Log.d("random1", ""+ Hex.bytesToHexString(random));
        return OK;
    }

    private int CTL_SelectAID_3000000() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmd = new byte[]{-112, 90, 0, 0, 3, 3, 0, 0, 0};
        rapdu=reader.sendCTL(cmd);
        if(rapdu==null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(rapdu) != 0){
            return ERR_SW1SW2;
        }
        return OK;
    }

    private int CTL_ReadCIStatus() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmdReadCIStatusBRIDesfire = new byte[]{-112, -67, 0, 0, 7, 1, 0, 0, 0, 0, 0, 0, 0};
        rapdu=reader.sendCTL(cmdReadCIStatusBRIDesfire);
        if(rapdu==null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(rapdu) != 0){
            return ERR_SW1SW2;
        }
        if(!(rapdu[3] == 0x61 && rapdu[4] == 0x61))
            return ErrorCode.BRI_CTL_ERR_APPNOTACTIVE;
        return OK;
    }

    private int CTL_ReadCardHolderDesfire() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmdReadCardHolderBRIDesfire = new byte[]{-112, -67, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0};
        rapdu=reader.sendCTL(cmdReadCardHolderBRIDesfire);
        if(rapdu==null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(rapdu) != 0){
            return ERR_SW1SW2;
        }
        cardNo=Arrays.copyOfRange(rapdu, 3, 11);
        Log.d("cardNo", ""+ Hex.bytesToHexString(cardNo));
        return OK;
    }

    private int CTL_AuthDesfire(byte[] random) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] apdu = new byte[]{-112, -81, 0, 0, 16};
        int idx = 0;
        byte[] authdata = new byte[8];
        byte[] command = new byte[apdu.length + random.length + 1];
        System.arraycopy(apdu, 0, command, idx, apdu.length);
        idx = idx + apdu.length;
        System.arraycopy(random, 0, command, idx, random.length);
        idx += random.length;
        command[idx++] = 0;
        rapdu = reader.sendCTL(command);
        if(rapdu==null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(rapdu) != 0){
            return ERR_SW1SW2;
        }

        System.arraycopy(rapdu, 0, authdata, 0, authdata.length);
        setAuthData(authdata);

        return 0;
    }

    byte[] authdata=new byte[8];
    private int setAuthData(byte[] authdata) {
        if(authdata.length!=8)
            return NOT_OK;
        this.authdata=authdata;
        return OK;
    }

    public int SAM_GetRandom(){
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmd = new byte[50];
        byte[] command ;
        byte[] random = new byte[16];
        int idx = 0;
        cmd[idx++] = -128;
        cmd[idx++] = -80;
        cmd[idx++] = 0;
        cmd[idx++] = 0;
        cmd[idx++] = 32;

        System.arraycopy(cardNo,0,cmd,idx,cardNo.length);
        idx+= cardNo.length;

        System.arraycopy(uid,0,cmd,idx,7);

        idx+=7;
        cmd[idx++] = -1;
        cmd[idx++] = 0;
        cmd[idx++] = 0;
        cmd[idx++] = 3;
        cmd[idx++] = 0;
        cmd[idx++] = -128;
        cmd[idx++] = 0;
        cmd[idx++] = 0;
        cmd[idx++] = 0;

        System.arraycopy(getRandom(),0,cmd,29,getRandom().length);
        idx+=getRandom().length;

        command = new byte[idx];
        System.arraycopy(cmd,0,command,0,idx);

        rapdu=reader.sendSAM(command);

        if(rapdu==null){
            return ErrorCode.BRI_SAM_ERR_GETRANDOM;
        }
        if(APDUHelper.CheckResult(rapdu) != 0){
            return ErrorCode.BRI_SAM_ERR_GETRANDOM;
        }

        System.arraycopy(rapdu,16,random,0,random.length);
        setRandom(random);

        return 0;
    }

    public int CTL_SelectAIDDesfire() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmdSelectAIDBRIDesfire = new byte[]{-112, 90, 0, 0, 3, 1, 0, 0, 0};
        rapdu=reader.sendCTL(cmdSelectAIDBRIDesfire);
//        Log.d("rapduselectaiddesfire",""+ Hex.bytesToHexString(rapdu));
        if(rapdu==null){
            return ErrorCode.BRI_CTL_ERR_SELECTAID;
        }
        if(APDUHelper.CheckResult(rapdu) != 0){
            return ErrorCode.BRI_CTL_ERR_SELECTAID;
        }
        return 0;
    }

    public int deduct(int sAmount) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int ldeductAmount;
        byte[] amount;
        int result=cekBalance();
        if(result!= OK)
        {
            return result;
        }

        ldeductAmount = Integer.valueOf(sAmount);
        amount= Converter.InttoByteArray(ldeductAmount, Converter.BIG_ENDIAN);
        if (saldoAwal < ldeductAmount) {
            return CTL_ERR_INSUFFICIENTBALANCE;}

        System.arraycopy(amount,0, baAmount,0,4);

        result = CTL_Debit(amount);
        if(result!=OK)
            return BRI_CTL_ERR_DESFIREDEBIT;

        String strTime = Util.dateNow("ddMMyyHHmmss");
        byte[] time =Hex.hexStringToByteArray(strTime);
        result=SAM_CreateHash( amount,strTime);
        if(result!=OK)
            return BRI_SAM_ERR_DESFIRECREATEHASH;

        result=CTL_WriteLog(baAmount,time);
        if(result!=OK) {
            if(result==ERR_SW1SW2) {
                CTL_AbortTransaction();
            }
            return BRI_CTL_ERR_DESFIREWRITELOG;
        }

        result=CTL_WriteLastTrx(baAmount,time);
        if(result!=OK) {
            if (result == ERR_SW1SW2) {
                CTL_AbortTransaction();
            }
            return BRI_CTL_ERR_DESFIREWRITELASTTRX;
        }

        result = CTL_CommitTransaction();
        //result=BRI_TestAutoCorrection();
        if (result != OK) {
            return BRI_CTL_ERR_DESFIRECOMMITTRX;
        }

        lBalanceKartu-=ldeductAmount;
        saldoAkhir=ldeductAmount;

        return result;
    }

    private int CTL_CommitTransaction() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        byte[] cmdCommitTrx=  {(byte) 0x90,(byte) 0xC7,(byte) 0x00,(byte) 0x00,(byte) 0x00};

        //result = SendCTL(cmdCommitTrx,sizeof(cmdCommitTrx),apdubuf,&apdulen/*,SW_9100*/);
        rapdu = reader.sendCTL(cmdCommitTrx);
        if(rapdu == null){
            return BRI_CTL_ERR_DESFIRECOMMITTRX;
        }
        if(APDUHelper.CheckResult(rapdu) != OK){
            CTL_AbortTransaction();
            //Log.d(TAG,"BRI_CTL_ERR_COMMIT - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return BRI_CTL_ERR_DESFIRECOMMITTRX;
        }


        return OK;
    }

    private int CTL_WriteLastTrx(byte[] amount, byte[] time) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int idx = 0;
        byte[] cmdSendToCL = new byte[255];
        byte[] command;
        byte[] cmdWriteLastTrx	=  {(byte) 0x90,(byte) 0x3D,(byte) 0x00,(byte) 0x00,(byte) 0x0E,(byte) 0x03,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x07,(byte) 0x00,(byte) 0x00};
        byte[] akumdebet= new byte[4];
        long total;
        Arrays.fill(cmdSendToCL, (byte) 0x00);
        System.arraycopy(cmdWriteLastTrx, 0, cmdSendToCL, idx, cmdWriteLastTrx.length);
        idx += cmdWriteLastTrx.length;

        System.arraycopy(time, 0, cmdSendToCL, idx, 3);
        idx += 3;
        if (lasttrxdate[1] != time[1]) {
            //memcpy(ctl_bri.akumdebet,amount,4);
            System.arraycopy(amount, 0, akumdebet, 0, 4);
        } else {
            total = lAkumDebet + Converter.ByteArrayToInt(amount,0,4,Converter.BIG_ENDIAN);
            akumdebet = Converter.IntegerToByteArray((int)total);
        }

        System.arraycopy(akumdebet, 0, cmdSendToCL, idx, 4);
        idx += 4;

        idx++;//for trailing zero
        command = new byte[idx];
        System.arraycopy(cmdSendToCL, 0, command, 0, command.length);
        rapdu = reader.sendCTL(command);
        if (rapdu == null) {
            return ERR_NO_RESP;
        }
        if (APDUHelper.CheckResult(rapdu) != OK) {
            CTL_AbortTransaction();
            return ERR_SW1SW2;
        }
        return OK;
    }

    private int CTL_AbortTransaction() {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        int result;
        byte[] cmdAbortTrx =  {(byte) 0x90,(byte) 0xA7,(byte) 0x00,(byte) 0x00,(byte) 0x00};

        rapdu = reader.sendCTL(cmdAbortTrx);
        if(rapdu == null){
            return ERR_NO_RESP;
        }
        if(APDUHelper.CheckResult(rapdu) != OK){
            //Log.d(TAG,"CTL_ERR_DESFIREABORTTRX - RAPDU: " + BytesUtil.bytes2HexString(RAPDU));
            return ERR_SW1SW2;
        }

        return  OK;
    }

    private int CTL_WriteLog(byte[] amount, byte[] time) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmdWriteLog=  {(byte) 0x90,(byte) 0x3B,(byte) 0x00,(byte) 0x00,(byte) 0x27,(byte) 0x01,(byte) 0x00,(byte) 0x00,(byte) 0x00,(byte) 0x20,(byte) 0x00,(byte) 0x00};
        int idx=0;
        byte[] cmdSendToCL = new byte[255];
        //byte[] merchantid;
        byte[] command;
        byte[] amount_reverse;
        byte[] balance_new = new byte[4];
        byte[] balance_old = new byte[4];
        Arrays.fill(cmdSendToCL, (byte) 0x00);
        System.arraycopy(cmdWriteLog, 0, cmdSendToCL, idx, cmdWriteLog.length);
        idx += cmdWriteLog.length;

        //merchantid = BytesUtil.hexString2Bytes(MID);

        System.arraycopy(devinfo.issuer.briMid, 0, cmdSendToCL, idx, devinfo.issuer.briMid.length);
        idx += devinfo.issuer.briMid.length;
        //memcpy(&cmdSendToCL[idx],&hash_terminalid[0],sizeof(hash_terminalid));
        System.arraycopy(issuer.str_briTid.getBytes(), 0, cmdSendToCL, idx, issuer.str_briTid.getBytes().length);
        idx += issuer.str_briTid.getBytes().length;



        System.arraycopy(time, 0, cmdSendToCL, idx, time.length);
        idx += time.length;
        cmdSendToCL[idx++] = (byte) 0xEB;


        amount_reverse=Converter.ChangeEndian(amount,4);
        System.arraycopy(amount_reverse, 0, cmdSendToCL, idx, 3);
        idx += 3;
        //memcpy(&cmdSendToCL[idx],&ctlinfo.bBalanceKartu,3);
        balance_old = Converter.ChangeEndian(bBalanceKartu,4);
        System.arraycopy(balance_old, 0, cmdSendToCL, idx, 3);
        idx += 3;

        //memcpy(&cmdSendToCL[idx],&balance_new,3);
        int newBalance = Integer.valueOf(saldoAwal) - Converter.ByteArrayToInt(amount,0,4,Converter.BIG_ENDIAN);
        balance_new = Converter.InttoByteArray(newBalance, Converter.LITTLE_ENDIAN);
        System.arraycopy(balance_new, 0, cmdSendToCL, idx, 3);
        idx += 3;

        idx++;//for trailing zero
        command = new byte[idx];
        System.arraycopy(cmdSendToCL, 0, command, 0, command.length);
        rapdu = reader.sendCTL(command);
        if (rapdu == null) {
            return ERR_NO_RESP;
        }
        if (APDUHelper.CheckResult(rapdu) != OK) {

            return ERR_SW1SW2;
        }
        return OK;
    }

    private int SAM_CreateHash(byte[] amount, String time) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmdSAMCreateHash=  {(byte) 0x80,(byte) 0xB4,(byte) 0x00,(byte) 0x00,(byte) 0x58};
        int idx=0;
        byte[] hash = new byte[4];
        byte[] cmdSendToSAM = new byte[255];

        byte[] padding ={(byte)0xFF,(byte)0xFF,(byte)0xFF,(byte)0xFF};
        byte[] command;
        long lAmt;

        hash_cardno = Hex.bytesToHexString(cardNo);

        timestamp = time;

        //2021-01-21,vivo, convert amount from hex to integer
        lAmt=Converter.ByteArrayToInt(amount,0,amount.length,Converter.BIG_ENDIAN);
        hash_amount = String.format("%08d", lAmt);
        //hash_amount = BytesUtil.bytes2HexString(amount);
        hash_amount+="00";//add decimal value
        hash_refno = this.refno;

        hash_batchno = Hex.bytesToHexString(issuer.briBatch);

        Arrays.fill(cmdSendToSAM, (byte) 0x00);
        idx = 0;

        System.arraycopy(cmdSAMCreateHash, 0, cmdSendToSAM, idx, cmdSAMCreateHash.length);
        idx += cmdSAMCreateHash.length;

        System.arraycopy(cardNo, 0, cmdSendToSAM, idx, cardNo.length);
        idx += cardNo.length;

        //System.arraycopy(passti.ctlinfo.uid.value, 1, cmdSendToSAM, idx, 7);
        System.arraycopy(uid, 0, cmdSendToSAM, idx, 7);//device return 7 bytes UID, and passti.ctlinfo.uid.value store 7 bytes
        idx += 7;

        System.arraycopy(cmddata, 0, cmdSendToSAM, idx, cmddata.length);
        idx += cmddata.length;

        System.arraycopy(authdata, 0, cmdSendToSAM, idx, 8);
        idx += 8;

        System.arraycopy(hash_cardno.getBytes(), 0, cmdSendToSAM, idx, hash_cardno.getBytes().length);
        idx += hash_cardno.getBytes().length;

        System.arraycopy(hash_amount.getBytes(), 0, cmdSendToSAM, idx, hash_amount.getBytes().length);
        idx += hash_amount.getBytes().length;

        System.arraycopy(timestamp.getBytes(), 0, cmdSendToSAM, idx, timestamp.getBytes().length);
        idx += timestamp.getBytes().length;

        System.arraycopy(issuer.briProcode, 0, cmdSendToSAM, idx, issuer.briProcode.length);
        idx += issuer.briProcode.length;

        System.arraycopy(hash_refno.getBytes(), 0, cmdSendToSAM, idx, hash_refno.getBytes().length);
        idx += hash_refno.getBytes().length;

        System.arraycopy(hash_batchno.getBytes(), 0, cmdSendToSAM, idx, hash_batchno.getBytes().length);
        idx += hash_batchno.getBytes().length;

        System.arraycopy(padding, 0, cmdSendToSAM, idx, padding.length);
        idx += padding.length;



        command = new byte[idx];
        System.arraycopy(cmdSendToSAM, 0, command, 0, command.length);
        rapdu = reader.sendSAM(command);
        if (rapdu == null) {
            return ERR_NO_RESP;
        }
        if (APDUHelper.CheckResult(rapdu) != OK) {

            return ERR_NO_RESP;
        }
        System.arraycopy(rapdu,0,hash,0,4);
        this.samHash=hash;

        return OK;
    }

    private int CTL_Debit(byte[] amount) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        byte[] cmdDebitBalanceBRIDesfire	=  {(byte) 0x90,(byte) 0xDC,(byte) 0x00,(byte) 0x00,(byte) 0x05};
        byte[] command=new byte[cmdDebitBalanceBRIDesfire.length+amount.length+1+1];
        byte[] le_amount;
        int idx=0;
        System.arraycopy(cmdDebitBalanceBRIDesfire, 0, command, idx, cmdDebitBalanceBRIDesfire.length);
        idx += cmdDebitBalanceBRIDesfire.length;
        command[idx++] = 0x00;
        le_amount=Converter.ChangeEndian(amount,4);//converto to little endian
        System.arraycopy(le_amount,0,command,idx,le_amount.length);
        idx+=le_amount.length;
        command[idx++] = 0x00;//trailing zero

        rapdu = reader.sendCTL(command);
        if (rapdu == null) {
//            passti.SetAction(params.REPURCHASE);
            return ERR_NO_RESP;
        }
        if (APDUHelper.CheckResult(rapdu) != OK) {
            return ERR_SW1SW2;
        }

        return OK;
    }

    public int getBalance() {
        return this.saldoAwal;
    }

    public int getDeduct() {
        return this.saldoAkhir;
    }

    public void setRefNo(String refno) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        Log.d(TAG,"trxrefno "+refno);

        int ref=Integer.parseInt(refno);
        Log.d(TAG,"ref "+ref);
        this.refno=refno;
//        setRefno(refno);
    }
}
