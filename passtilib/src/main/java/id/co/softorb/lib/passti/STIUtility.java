package id.co.softorb.lib.passti;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialDriver;

import id.co.softorb.lib.helper.Converter;
import id.co.softorb.lib.litro.Params;
import id.co.softorb.lib.litro.Reader;

import static android.content.ContentValues.TAG;
import static id.co.softorb.lib.helper.ErrorCode.ERR_CARDTYPEINACTIVE;
import static id.co.softorb.lib.helper.ErrorCode.OK;

public class STIUtility {
    Context ctx;
    Reader reader;
    BRI bri;
    String tagclass= this.getClass().getSimpleName();

    int card = 0;
    int saldoAwal, saldoAkhir;

    public STIUtility(Context ctx){
        this.ctx=ctx;
        reader=new Reader(ctx);
    }

    public int initReader(UsbManager usbManager, UsbDevice usbDevice, UsbSerialDriver driver) {
        return reader.init(usbManager, usbDevice, driver);
    }

    public int cekBalance() {
        bri=new BRI(ctx, reader);
        int i = ERR_CARDTYPEINACTIVE;
        i=bri.CTL_SelectAIDDesfire();
        Log.d("brii", ""+i);
        if(i==OK){
            i=bri.cekBalance();
        }
        return i;
    }

    public int deduct(int amount) {
        Log.d("nominal", ""+amount);
        int i = ERR_CARDTYPEINACTIVE;
        i=bri.CTL_SelectAIDDesfire();
        Log.d("brii", ""+i);
        if(i==OK){
            i=bri.deduct(amount);
        }
        return i;
    }

    public int getBalance() {
        saldoAwal=bri.getBalance();
        return saldoAwal;
    }

    public int getDeduct() {
        saldoAkhir=bri.getDeduct();
        return saldoAkhir;
    }

    public void setTrxCounter(int counter) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        devinfo.trxno= Converter.InttoByteArray(counter,Converter.BIG_ENDIAN);
    }

    public void setBRIRefNo(String refno) {
        Log.i(TAG, tagclass+"."+new Throwable()
                .getStackTrace()[0]
                .getMethodName());
        bri.setRefNo(refno);

    }
}
